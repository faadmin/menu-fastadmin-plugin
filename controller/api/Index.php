<?php

namespace addons\sdcmenu\controller\api;

use addons\sdcmenu\library\captcha\CaptchaBuilder;
use fast\Date;
use fast\Random;
use think\Cache;

class Index extends Api
{
    protected $noNeedRight = ['*'];
    protected $noNeedLogin = ['*'];
    public function index()
    {
        return Cache::get('captcha_gLqEUtj2ZY');
    }

    public function captcha()
    {
        $captch = new CaptchaBuilder();
        $captch->initialize([
            'width' => 150,     // 宽度
            'height' => 50,     // 高度
            'line' => false,    // 直线
            'curve' => false,    // 曲线
            'noise' => 1,       // 噪点背景
            'fonts' => []       // 字体
        ]);
        $captch->create();
        $captchKey = 'captcha_'.Random::alnum(10);
        Cache::set($captchKey,$captch->getText(),Date::HOUR);
        $this->success(__('Request success'),[
            'captcha_key' => $captchKey,
            'image' => $captch->getBase64()
        ]);
    }

}
